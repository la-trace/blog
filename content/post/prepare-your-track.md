---
title: "Tracez vos GPX !" 
date: 2019-08-01T00:00:24+01:00 
bigimg: [{src: "/img/prepare-screenshot.jpg", desc: ""}]
draft: false
---

# Préparer vos traces GPX !

> Vous avez désormais la possibilité de tracer vos itinéraires GPX avec
la-trace.com. [https://la-trace.com/fr/prepare](https://la-trace.com/fr). 

<!--more-->

![tracer vos gpx](/img/screencast.gif)

## Editez n'importe quelle trace

Depuis la page de détail d'une trace, vous pouvez désormais cliquer sur le
bouton éditer afin de modifier une trace avec les outils mis à votre
disposition. Vous pouvez choisir entre un fond de carte OpenStreetMap ou IGN.

## Tracez, Coupez, Fusionnez, Inverser les traces !

### Les outils de positionnement

Grâce à l'outil de reverse geocoding, Vous pouvez taper le nom d'une ville afin
de centrer la carte sur cette localisation. Un bouton est également disponible
pour essayer de déterminer votre position actuelle et centrer la carte sur
celle-ci.

### Les outils de traçage

Disponible sur la gauche de la carte, les outils de traçage vous permettent de
tracer un itinéraire sur le fond de carte. Vous pouvez **déplacer** les points
qui constituent cette trace, ainsi que les **supprimer**.

Lorsque vous modifiez votre trace, le **kilométrage** se met à jour **en temps
réél** sur la droite de votre écran.

Vous pouvez également **prolonger** une trace en cliquant sur le premier et le
dernier point de la trace. 

Vous avez la possibilité d'**extraire un segment** de trace en coupant
celle-ci. Cette fonctionnalité est très pratique pour ne prendre qu'une partie
de trace et ajuster l'itinéraire initial à votre guise.

### Les outils globaux

L'application met à disposition des outils pour **inverser** le sens d'une
trace, **fusionner** des segments entre eux. Vous pouvez ajuster la carte pour
la **centrer** sur le segment sélectionné. Enfin vous pouvez **télécharger**
votre trace sour la forme d'un fichier GPX.  

## Heatmap

![Heatmap ou carte de densité](/img/heatmap.jpg)

Fonctionnalité encore expérimentale, la **heatmap** – ou carte de densité –
affiche sur la carte les points sous forme de zone de chaleur. Au plus une zone
est fréquentée par les utilisateurs du site, au plus elle apparaîtra en rouge.

Cette fonctionnalité permet de voir les zones empreuntées pour tracer plus
facilement et plus rapidement vos itinéraires. Techniquement ceci est permis
par la base de donnée PostGIS, [plus de détail ici]({{< relref
"v3-technique-under-the-hood.md" >}}) qu'utilise le site suite à la refonte.

Etant assez gourmande en ressource, elle est limitée à des niveaux de zoom
élevés afin de ne pas surcharger le serveur.

## Conclusion 

Il s'agit d'un premier jet de cet outil de traçage, il sera amener à évoluer
dans un futur proche. Si vous constater des bugs ou vous avez des idées
d'amélioration, n'hésitez pas à me contacter via le formulaire sur le site ou
via [twitter](https://twitter.com/latracepointcom).